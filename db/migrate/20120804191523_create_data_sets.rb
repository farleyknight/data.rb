class CreateDataSets < ActiveRecord::Migration
  def change
    create_table :data_sets do |t|
      t.text :column_names
      t.text :data

      t.timestamps
    end
  end
end
