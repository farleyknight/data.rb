class RenameCsvToCsvFileOnDataSets < ActiveRecord::Migration
  def up
    rename_column :data_sets, :csv, :csv_file
  end

  def down
  end
end
