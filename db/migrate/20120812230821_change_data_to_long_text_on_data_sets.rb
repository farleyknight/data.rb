class ChangeDataToLongTextOnDataSets < ActiveRecord::Migration
  def up
    change_column :data_sets, :data, :text, :limit => 4294967295
  end

  def down
  end
end
