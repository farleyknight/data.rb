class AlgorithmsController < ApplicationController
  def linear_regression
    @data_set = DataSet.find(params[:id])

    respond_to do |format|
      format.json do
        render :json => {
          :model => @data_set.linear_regression.model
        }
      end

      format.html do
        render :action => "linear_regression"
      end
    end
  end
end
