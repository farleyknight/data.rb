class DataSetsController < ApplicationController
  def index
    @data_sets = DataSet.all
  end

  def new
    @data_set = DataSet.new
  end

  def show
    @data_set = DataSet.find(params[:id])

    respond_to do |format|
      format.html do
        render :action => "show"
      end
    end
  end

  def compare
    @data_set = DataSet.find(params[:id])

    respond_to do |format|
      format.json do
        render :json => @data_set.compare(params[:r], params[:c])
      end
    end
  end

  def chart
    @data_set = DataSet.find(params[:id])

    if @data_set.regression?
      render :json => {
        :chart => @data_set.chart,
        :model => @data_set.linear_regression.model
      }
    end
  end

  def multichart
    @data_set = DataSet.find(params[:id])

    if @data_set.regression?
      render :json => {
        :chart => @data_set.multichart,
        :model => @data_set.linear_regression.multimodel
      }
    elsif @data_set.categorical?
      render :json => @data_set.multichart
    end
  end

  def create
    @data_set = DataSet.create(params[:data_set])
    redirect_to :action => "show", :id => @data_set
  end

  def edit
    @data_set = DataSet.find(params[:id])
  end

  def update
    @data_set = DataSet.find(params[:id])
    @data_set.update_attributes(params[:data_set])
    redirect_to :action => "show", :id => @data_set
  end

  def destroy
    @data_set = DataSet.find(params[:id])
    @data_set.destroy
    redirect_to :action => "index"
  end
end
