(function($) {
  $(function() {
    $("#data_table").dataTable({
      "bJQueryUI"       : true,
      "sPaginationType" : "full_numbers"
    });

    var dataSetId = $("#data_table").attr('data_set_id');
   
    if ($("#chart").exists()) {
      $.get("/data_sets/" + dataSetId + "/chart/", function(data) {
        var chart = data['chart'];
        $.jqplot('chart', [chart['data'], data['model']], {
          series : [
            {showLine: false, showMarker: true}, 
            {showLine: true,  showMarker: false}
          ],
          axes : {
            xaxis : {
              label : chart['x_axis']
            },
            yaxis : {
              label : chart['y_axis']
            }
          }
        });
      });
    }

    if ($("#multichart").exists()) {
      $.get("/data_sets/" + dataSetId + "/multichart/", function(multichart) {
        $.each(multichart, function(r, row) {
          $.each(row, function(c, chart) {
            if (r != c) {
              var data = [];
              $.each(chart, function(i, values) {
                data[i] = {
                  data   : values['data'], 
                  lines  : {show: false}, 
                  points : {show: true, radius: 1}
                };
              });

              $.plot($("#chart_" + r + "_" + c), data, {
                yaxis: {labelWidth: 15}
              });
            }
          });
        });
      });    
    }
  });
})(jQuery);
