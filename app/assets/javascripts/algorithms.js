(function($) {
  var dataSetId;

  $(function() {
    if ($("#logistic_regression").exists()) {
      dataSetId = $("#logistic_regression").attr('data_set_id');

      $.get("/algorithms/" + dataSetId + "/linear_regression.json", function(data) {

      });
    }

    if ($("#linear_regression").exists()) {
      if ($("#multichart").exists()) {
        $.get("/algorithms/" + dataSetId + "/linear_regression.json", function(multichart) {
          
          $.each(multichart, function(r, row) {
            $.each(row, function(c, chart) {
              if (r != c) {
                var data = [{
                  data   : chart['data'],
                  lines  : {show: false}, 
                  points : {show: true, radius: 1}
                }, {
                  data   : chart['model'], 
                  lines  : {show: true},
                  points : {show: false}
                }];
                
                $.plot($("#chart_" + r + "_" + c), data, {
                  yaxis: {labelWidth: 15}
                });
              }
            });
          });          
        });
      }
    }
  });
})(jQuery);

