require 'csv'
#
# NOTE (farleyknight@gmail.com): This class will probably become
# a mere interface overtime..
#
# Anything that has a set of columns & a collection of instances
# is technically a 'dataset', so obviously any ActiveRecord
# class fits the bill..
#
class DataSet < ActiveRecord::Base
  PROBLEM_TYPES = %w(categorical regression clustering)

  def columns
    CSV.parse(column_names).first
  end

  def column_range
    (0..column_range_min)
  end

  def column_range_min
    [columns.size-1, 6].min
  end

  def size
    @size ||= data.size
  end

  alias :m :size

  def categories
    unless self[:categories].blank?
      CSV.parse(self[:categories]).first.map {|c| c.strip }
    else
      []
    end
  end

  def raw_data
    CSV.parse(self[:data])
  end

  def data
    raw_data.map do |row|
      row.map do |cell|
        if cell =~ /[0-9]*\.[0-9]*/
          cell.to_f
        elsif cell =~ /[0-9]*/
          cell.to_i
        else
          cell
        end
      end
    end
  end

  def matrix
    NMatrix[*data]
  end

  def regression?
    problem_type == "regression"
  end

  def categorical?
    problem_type == "categorical"
  end

  ######################
  # Data Visualization #
  ######################

  def chart
    formatted_data = data.map do |row|
      row.map do |cell|
        cell.to_f
      end
    end

    {
      :data   => formatted_data,
      :x_axis => columns.first,
      :y_axis => columns.second
    }
  end

  def data_by_category(category)
    data.select do |row|
      row.last.strip == category.strip
    end
  end

  # NOTE (farleyknight@gmail.com): This method will provide a 2D matrix
  # of values, comparing row = r & column = c, filtering on the
  # category
  def compare_by_category(r, c, category)
    data_by_category(category).map do |row|
      [row[c.to_i].to_f, row[r.to_i].to_f]
    end
  end

  def compare_without_category(r, c)
    data.map do |row|
      [row[c.to_i].to_f, row[r.to_i].to_f]
    end
  end

  # NOTE (farleyknight@gmail.com): Does the same as above, but against
  # all categories. Used for spitting out some JSON to the UI
  def compare(r, c)
    if categorical?
      categories.map do |cat|
        {
          :category => cat,
          :data     => compare_by_category(r, c, cat)
        }
      end
    else
      [{:data => compare_without_category(r, c)}]
    end
  end

  def multichart
    column_range.map do |r|
      column_range.map do |c|
        unless r == c
          compare(r, c)
        else
          columns[r]
        end
      end
    end
  end

  ##########
  # Models #
  ##########

  def linear_regression
    model = Algorithms::LinearRegression.new
    model.fit(x_padded, y)
    model
  end

  def x
    @x ||= matrix[0..-2, 0..-1]
  end

  # Pad the X observation matrix with a column of ones on the left
  # for the 0th coefficient:
  #
  # c0 + c1*x1 + c2*x2 + ...
  #
  def x_padded
    ones = NMatrix.ones(1,m).to_a.flatten
    NMatrix[*([ones] + x.transpose.to_a)].transpose
  end

  def y
    matrix[-1, 0..-1]
  end
end
