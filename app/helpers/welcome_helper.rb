module WelcomeHelper
  def iris_dataset(r, c, opts = {})
    CSV.read("vendor/data/iris.txt").select do |row|
      row.last == opts[:category]
    end.map do |row|
      [row[r].to_f, row[c].to_f]
    end
  end
end
