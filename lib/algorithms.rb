module Algorithms
  class LinearRegression
    attr_accessor :x, :y, :theta, :initial_cost, :final_cost

    ####################
    # Side Effect Free #
    ####################

    def self.cost(x, y, theta)
      d = (x*theta) - y
      m = x.shape[1]

      c = (1.0/2*m) * (d.transpose * d)
      c.to_a.flatten.first
    end

    def cost(x, y, theta)
      self.class.cost(x, y, theta)
    end

    def self.h(theta, x)
      theta * x
    end

    def h(theta, x)
      self.class.h(theta, x)
    end

    def self.alpha
      0.01
    end

    def alpha
      self.class.alpha
    end

    def self.iterations
      1500
    end

    def iterations
      self.class.iterations
    end

    def self.steps
      100
    end

    def steps
      self.class.steps
    end

    ############
    # Stateful #
    ############

    def n
      @n ||= @x.shape[0]
    end

    def m
      @m ||= @x.shape[1]
    end

    def fit(x, y)
      @x, @y        = x, y
      @theta        = NMatrix.zeros(1, n)

      @initial_cost = cost(@x, @y, @theta)
      grad_descent!
      @final_cost   = cost(@x, @y, @theta)
    end

    def grad_descent!
      @cost_history = []
      m             = @x.shape[1]
      coeff         = alpha/m
      theta         = NMatrix.zeros(1, n)

      (1..iterations).each do |i|
        d     = (@x*theta) - @y
        sum   = (d.transpose * @x)
        theta = theta - (coeff * sum.transpose)

        @cost_history << cost(@x, @y, theta)
      end

      @theta = theta
    end

    def coefficients
      @theta.last
    end

    def intercept
      @theta.first
    end

    def predict(x)
      (NMatrix[[1, x]] * @theta).to_a.flatten.first
    end

    def x_max
      @x.max
    end

    def x_min
      @x.min
    end

    def step
      @step ||= (@x.max - @x.min) / steps.to_f
    end

    def model
      data = []

      x = x_min

      while x <= x_max
        data << [x, predict(x)]
        x += step
      end

      data
    end
  end
end
