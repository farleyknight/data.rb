DataRb::Application.routes.draw do
  resources :data_sets do
    member do
      get :chart
      get :multichart
    end
  end

  match '/algorithms/:id/linear_regression'   => 'algorithms#linear_regression',   :as => :linear_regression

  match '/algorithms/:id/logistic_regression' => 'algorithms#logistic_regression', :as => :logistic_regression

  root :to => 'welcome#index'
end
