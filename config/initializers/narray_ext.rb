
NMatrix.class_eval do
  def col(i)
    self[i, 0..-1]
  end

  def self.ones(n, m)
    int(n, m).fill!(1)
  end

  def self.zeros(n, m)
    int(n, m).fill!(0)
  end

  alias :tran :transpose
end
